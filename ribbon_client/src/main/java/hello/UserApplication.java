package hello;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.List;

@SpringBootApplication
@RestController
@EnableDiscoveryClient
public class UserApplication {
    /*
    @Autowired
    private DiscoveryClient discovery = null;

    @Autowired
    private LoadBalancerClient balancerClient = null;

    private ServiceInstance serviceInstance = null;
    */
    @LoadBalanced
    @Bean
    RestTemplate restTemplate(){
        return new RestTemplate();
    }

    @RequestMapping("/{appClient}/{method}")
    public String hi(@PathVariable(name = "appClient") String appClient,
                     @PathVariable(name = "method") String method) {
/*
        //DISCOVERY DIRECTO
        List instances = discovery.getInstances(appClient);
        if(instances != null && instances.size() > 0) {
            serviceInstance = (ServiceInstance) instances.get(0);
            System.out.println(serviceInstance.getHost() + ":" + serviceInstance.getPort());

        }else
        {System.out.println("NULL");}
*/
        /*
        //LOADBALANCER
        serviceInstance = balancerClient.choose(appClient);
        if(serviceInstance != null) {
            System.out.println(serviceInstance.getHost() + ":" + serviceInstance.getPort());
        }*/


        URI productUri = URI.create(String
                .format("http://%s/%s", appClient, method));

        String greeting = this.restTemplate().getForObject(productUri, String.class);


    //String greeting = this.restTemplate().getForObject("http://"+ appClient + "/" + method, String.class);

        return greeting;
    }



    public static void main(String[] args) {
        SpringApplication.run(UserApplication.class, args);
    }
}
